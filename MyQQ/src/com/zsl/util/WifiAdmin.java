package com.zsl.util;

import java.util.List;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;

/**
 * Wifi 工具类
 * @author ZSl
 * @version 2014年5月16日 14:01:45
 */
public class WifiAdmin {
	// 定义一个WifiManager
	private WifiManager wifiManager;
	// 定义一个wifiinfo对象
	private WifiInfo wifiInfo;
	// 扫描出的Wifi列表
	private List<ScanResult> wList;
	// 网络连接列表
	private List<WifiConfiguration> wConfigList;

	private WifiLock wifiLock;

	public WifiAdmin(Context context) {
		super();
		// 获取到Wifimanager对象
		this.wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		// 获取到wifiinfo对象
		this.wifiInfo = wifiManager.getConnectionInfo();
	}

	// 打开wifi
	public void openWifi() {
		if (!wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(true);
		}
	}

	// 关闭wifi
	public void closeWifi() {
		if (wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(false);
		}
	}

	// 检查当前Wifi状态
	public int checkState() {
		return wifiManager.getWifiState();
	}

	// 锁定Wifilock
	public void acquireWifiLock() {
		wifiLock.acquire();
	}

	// 解锁wifiLock
	public void releaseWifiLock() {
		// 判断当前Wifi是否锁定
		if (wifiLock.isHeld()) {

			wifiLock.release();
		}
	}

	// 创建一个wifilock
	public void createWifiLock() {
		wifiLock = wifiManager.createWifiLock("test");
	}

	// 得到配置好的网络
	public List<WifiConfiguration> getConfiguration() {
		return wConfigList;
	}

	// ===========这里可配置==============
	// 指定配置好的网络进行连接
	public boolean connnectionConfiguration(int index) {
		if (index > wConfigList.size()) {
			return false;
		}
		// 连接配置好指定ID的网络
		return wifiManager.enableNetwork(wConfigList.get(index).networkId, true);
	}

	// 扫描wifi
	public void startScan() {
		// 执行扫描
		wifiManager.startScan();
		// 获得扫描得到的结果
		wList = wifiManager.getScanResults();
		// 得到配置好的网络连接
		wConfigList = wifiManager.getConfiguredNetworks();
	}
	
//	得到网络列表
	public List<ScanResult> getWifiList(){
		return wList;
	}
	
//	查看扫描结果
	public StringBuffer lookUpScan(){
		StringBuffer sb=new StringBuffer();
		for (int i = 0; i < wList.size(); i++) {
			sb.append("index_"+new Integer(i+1).toString()+":");
			sb.append((wList.get(i)).toString()).append("\n");
		}
		return sb;
	}
//	得到wifi的mac地址
	public String getMacAddress(){
		return (wifiInfo==null)?"null":wifiInfo.getMacAddress();
	}

//	得到wifi的Bssid
	public String getBSSID(){
		return (wifiInfo==null)?"null":wifiInfo.getBSSID();
	}
	
//	得到wifi的ip地址
	public int getIPAdress(){
		return (wifiInfo==null)?0:wifiInfo.getIpAddress();
	}
	
//	得到连接的ID
	public int getNetWordid(){
		return (wifiInfo==null)?0:wifiInfo.getNetworkId();
	}
	
//	得到wifiinfo的所有信息
	public String getWifiInfo(){
		return (wifiInfo==null)?"null":wifiInfo.toString();
	}
	
//	添加一个网络并连接
	public boolean addNetWork(WifiConfiguration configuration){
		int wfgld=wifiManager.addNetwork(configuration);
		System.out.println("=====添加一个Wifi====");
		return wifiManager.enableNetwork(wfgld, true);
		
	}
	
//	断开指定id的网络
	public void disConnectionWifi(int netId){
		wifiManager.disableNetwork(netId);
		wifiManager.disconnect();
	}
}
