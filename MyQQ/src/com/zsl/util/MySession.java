package com.zsl.util;


import java.util.HashMap;

import java.util.Map;
/**
 * 自定义的Session用于存储对象
 * @author ZSL
 * @version 2014-6-16 16:36:58
 */

public class MySession {

	@SuppressWarnings("unchecked")
	private Map _objectContainer;

	private static MySession MySession;

	// Attention here, DO NOT USE keyword 'new' to create this object.

	// Instead, use getSession method.

	@SuppressWarnings("unchecked")
	private MySession() {

		_objectContainer = new HashMap();

	}


	@SuppressWarnings("unchecked")
	public void put(Object key, Object value) {

		_objectContainer.put(key, value);

	}

	public Object get(Object key) {

		return _objectContainer.get(key);

	}

	public void cleanUpMySession() {

		_objectContainer.clear();

	}

	public void remove(Object key) {

		_objectContainer.remove(key);

	}

	public static MySession getMySession() {
		if (MySession == null) {

			MySession = new MySession();

			return MySession;

		} else {

			return MySession;

		}
	}

}
