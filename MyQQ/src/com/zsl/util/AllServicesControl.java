package com.zsl.util;

import android.content.Intent;

public class AllServicesControl {
	/**
	 * 启动聊天消息服务
	 * @return 返回Intent对象
	 */
	public Intent startChatMsgServices(){
		Intent intent=new Intent();
		return intent.setAction("com.zsl.service.CHATMESSAGE_SERVICE");
	}
}
