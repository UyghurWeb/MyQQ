package com.zsl.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper{
	private final static String dbName="msg.db";
	private final static int version=1;

	public DBConnection(Context context) {
		super(context,dbName, null, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String sql="create table msg(" +
				"mid Integer primary key autoincrement," +
				"mto varchar(255) not null,"+
				"mfrom varchar(255) not null," +
				"msg varchar(255) not null," +
				"mdate varchar(255) not null," +
				"mstart Integer" +
				")";
		
		try {
			db.execSQL(sql);
			System.out.println("数据库创建成功。");
		} catch (SQLException e) {
			System.out.println("数据库创建失败。");
			e.printStackTrace();
			
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		// TODO Auto-generated method stub
		db.execSQL("drop table message");
		onCreate(db);
	}

}
