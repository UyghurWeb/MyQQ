package com.zsl.services;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zsl.adapter.MsgAdapter;
import com.zsl.bean.Msg;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;

import com.zsl.dao.MessageDao;
import com.zsl.dao.impl.MessageDaoImpl;
import com.zsl.myqq.LoginActivity;
import com.zsl.myqq.MsgActivity;
import com.zsl.myqq.R;
import com.zsl.smack.Smack;
import com.zsl.util.MySession;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ChatMessage extends Service {
	private Smack smack=LoginActivity.getSmack();
	private XMPPConnection xmppconn;
	private MessageDao messageDao;
	private Context context;
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	//服务第一次创建时调用的方法
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		MySession mySession=MySession.getMySession();
		context=(Context) mySession.get("context");
		messageDao=new MessageDaoImpl(context);
		super.onCreate();
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		new Thread(){
			public void run() {
				xmppconn=smack.getConnection();
				if (xmppconn.isConnected()&&xmppconn.isAuthenticated()) {
					List<RosterEntry> entries=smack.getEntries();
					for (int i = 0; i < entries.size(); i++) {
						String uname=entries.get(i).getUser().toString();
						xmppconn.getChatManager().createChat(uname, new MessageListener() {
							@Override
							public void processMessage(Chat chat, Message msg) {
								// TODO Auto-generated method stub
								System.out.println("services:other==message: " + msg.getBody());
								Msg msgs=new Msg();
								StringBuffer buffer=new StringBuffer(msg.getFrom().toString());
								String from=buffer.substring(0, buffer.lastIndexOf("/"));
								
								String to=msg.getTo().toString();
								System.out.println("=========StringBuffer:from:"+from+"to:"+to);
								msgs.setMfrom(from);
								msgs.setMto(to);
								msgs.setMsg(msg.getBody());
								msgs.setMstart(0);
								SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
								msgs.setMdate(dateFormat.format(new Date()).toString());
								System.out.println("======services:"+dateFormat.format(new Date()).toString());
								if (messageDao.save(msgs)) {
									System.out.println("=====services: 添加成功");
								}else{
									System.out.println("====services: 添加失败");
								}
								
								
							}
						 });
					}
				}
			};
		}.start();
		return super.onStartCommand(intent, flags, startId);
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	public class MyBinder extends Binder{
		public ChatMessage getChatMessage(){
			return ChatMessage.this;
		}
	}
	
	
}
