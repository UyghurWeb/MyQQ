package com.zsl.smack;

import java.util.HashMap;
import java.util.List;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.packet.VCard;

/**
 * 定义一个操作Smack的方法
 * @author ZSL
 * @version 2014-6-12 14:30:44
 */
public interface Smack {
	/**
	 * 登录
	 * @param name 用户名
	 * @param pass 密码
	 * @return 
	 * @throws XMPPException 异常
	 */
	public boolean login(String name,String pass)throws XMPPException;
	/**
	 * 登出
	 * @return 
	 */
	public boolean logout();
	/**
	 * 判断服务器是否连接成功
	 * 
	 */
	public boolean isAuthenticated();
	/**
	 * 注册
	 * @return 1、注册成功 0、服务器没有返回结果2、这个账号已经存在3、注册失败
	 */
	public int register(String name,String pass);
	/**
	 * 获取所有组
	 * @return
	 */
	public List<RosterGroup> getRosterGroup();
	/**
	 *获取每个组的好友
	 * @param groupName
	 */
	public List<RosterEntry> getEntriesByGroup(String groupName);
	/**
	 * 获取所有好友
	 * @return
	 */
	public List<RosterEntry> getEntries();
	/**
	 * 获取连接对象
	 * @return
	 */
	
	public XMPPConnection getConnection();
	/**
	 * 查询组
	 * @return
	 */
	public boolean getGroup(String Gname);
	/**
	 * 添加一个组
	 * @return
	 */
	public boolean addGroup(String groupName);
	
	/**
	 * 添加好友
	 * @param user 好友的UID
	 * @param name 好友的名字
	 * @param groupName 添加到得小组名字
	 * @return 
	 */
	public boolean addRoster(String user,String name, String groupName);
	/**
	 * 查询用户
	 * @param name 用户名
	 * @return
	 */
	public List<HashMap<String , Object>> searchUser(String name);
	/**
	 * 修改密码
	 * @param pwd 新密码
	 * @return
	 */
	public boolean changePwd(String pwd);
	/**
	 * 获取用户信息
	 * @param user 用户的user
	 * @return VCard 对象
	 */
	public VCard getVCard(String user);
	/**
	 * 获取用户
	 * @return
	 */
	public RosterEntry getRosterEntry(String name);
}
