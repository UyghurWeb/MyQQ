package com.zsl.myqq;

import com.zsl.smack.Smack;
import com.zsl.smack.SmackImpl;
import com.zsl.util.Valdate;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RegActivity extends Activity implements OnClickListener{
	private EditText et_name;
	private EditText et_pass;
	private EditText et_repass;
	private Button bt_reg;
	private ImageView top_iv_login;
	private TextView top_tv_login;
	private LayoutInflater inflater;
	private Smack smack=new LoginActivity().getSmack();
	private Handler handler;
	private Dialog dialog;
	
	@SuppressLint("HandlerLeak")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reg);
		init();
		bt_reg.setOnClickListener(this);
		top_iv_login.setOnClickListener(this);
		top_tv_login.setOnClickListener(this);
		inflater=LayoutInflater.from(this);
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
//				密码不一致
				case -2:
				{
					Toast.makeText(RegActivity.this, "两次密码输入不一致", Toast.LENGTH_LONG).show();
				}
				break;
//				提示消息
				case -1:
				{
					dialog=new Dialog(RegActivity.this);
					dialog.setTitle("注册提示");
					RelativeLayout ll=(RelativeLayout) inflater.inflate(R.layout.toast, null);
					TextView tv=(TextView) ll.findViewById(R.id.toast_text);
					tv.setText("注册中...");
					dialog.setContentView(ll);
					dialog.show();
				}
				break;
//				服务器未响应
				case 0:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(RegActivity.this, "服务器未响应", Toast.LENGTH_LONG).show();
				}
				break;
//				注册成功
				case 1:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(RegActivity.this, "注册成功", Toast.LENGTH_LONG).show();
				}
				break;
//				用户名已存在
				case 2:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(RegActivity.this, "用户名已存在", Toast.LENGTH_LONG).show();
				}
				break;
//				注册失败
				case 3:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(RegActivity.this, "注册失败", Toast.LENGTH_LONG).show();
				}
				break;
//				帐号不能为空
				case 4:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(RegActivity.this, "帐号不能为空", Toast.LENGTH_LONG).show();
				}
				break;
//				密码不能为空
				case 5:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(RegActivity.this, "密码不能为空", Toast.LENGTH_LONG).show();
				}
				break;
				default:
					break;
				}
			}
		};
	}
	
	
	/**
	 * 控件初始化
	 */
	private void init(){
		et_name=(EditText) findViewById(R.id.reg_et_name);
		et_pass=(EditText) findViewById(R.id.reg_et_pass);
		et_repass=(EditText) findViewById(R.id.reg_et_repass);
		bt_reg=(Button) findViewById(R.id.reg_bt_reg);
		top_iv_login=(ImageView) findViewById(R.id.reg_top_iv_login);
		top_tv_login=(TextView) findViewById(R.id.reg_top_tv_login);
	}

	/**
	 * 点击事件
	 */
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		//注册
		case R.id.reg_bt_reg:
		{
			final String name=et_name.getText().toString();
			final String pass=et_pass.getText().toString();
			String repass=et_repass.getText().toString();
			System.out.println(repass+":"+pass);
			Valdate valdate=new Valdate();
			if(valdate.valdates(name)){
				
			if (valdate.valdates(pass)&&valdate.valdates(repass)) {
				if (repass.equals(pass)) {
					
					new Thread(){
						
						public void run() {
							handler.sendEmptyMessage(-1);
							int result=smack.register(name, pass);
							handler.sendEmptyMessage(result);
						};
						
					}.start();
				}else{
					handler.sendEmptyMessage(-2);
				}
			} else {
				handler.sendEmptyMessage(5);
			}
			
			}else{
				handler.sendEmptyMessage(4);
			}
		}
		break;
//		返回登陆界面
		case R.id.reg_top_iv_login:
		{
			Intent intent=new Intent(RegActivity.this, LoginActivity.class);
			
			startActivity(intent);
			this.finish();
		}
		break;
		
//		返回登陆界面
		case R.id.reg_top_tv_login:
		{
			Intent intent=new Intent(RegActivity.this, LoginActivity.class);
			
			startActivity(intent);
			this.finish();
		}
		break;
		default:
			break;
		}
	}
	
	/**
	 * 返回到登录页面
	 * 
	 */
	@Override
	public void onBackPressed() {
		startActivity(new Intent(this, LoginActivity.class));
		this.finish();
	}

	
}
