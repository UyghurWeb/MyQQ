package com.zsl.myqq;


import com.zsl.util.AllServicesControl;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
/**
 * tab_mian 主界面
 * @author ZSL
 * @version 2014-06-08 12:43:22
 */
public class TabMainActivity extends TabActivity{
	private LayoutInflater inflater;
	private AllServicesControl allServicesControl=new AllServicesControl();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inflater=LayoutInflater.from(this);
		startService(allServicesControl.startChatMsgServices());
		TabHost tabHost=getTabHost();
		LayoutInflater.from(this).inflate(R.layout.tab_main, tabHost.getTabContentView(), true);
		tabHost.addTab(tabHost.newTabSpec("a")
				.setIndicator(getView(R.drawable.tab_item_msg, "消息"))
				.setContent(new Intent(TabMainActivity.this, MsgActivity.class)));
		
		tabHost.addTab(tabHost.newTabSpec("a1")
				.setIndicator(getView(R.drawable.tab_item_contants, "联系人"))
				.setContent(new Intent(TabMainActivity.this, ContantsActivity.class)));
		
		tabHost.addTab(tabHost.newTabSpec("a2")
				.setIndicator(getView(R.drawable.tab_item_tools, "工具"))
				.setContent(new Intent(TabMainActivity.this,ToolsActivity.class)));
		
		
		tabHost.addTab(tabHost.newTabSpec("a3")
				.setIndicator(getView(R.drawable.tab_item_me, "我"))
				.setContent(new Intent(TabMainActivity.this,MeActivity.class)));
		
	}
	/**
	 * 自定义tab_item样式
	 * @param icon 图标路径
	 * @param content 文字内容
	 * @return View
	 */
	
	private View getView(int icon,String content){
		LinearLayout ll=(LinearLayout) inflater.inflate(R.layout.tab_item, null);
		ImageView iv_icon=(ImageView) ll.findViewById(R.id.tab_item_iv_icon);
		iv_icon.setImageResource(icon);
		TextView tv_content=(TextView) ll.findViewById(R.id.tab_item_tv_content);
		tv_content.setText(content);
		
		return ll;
	}
	
	@Override
	public void onBackPressed() {
		
		Intent intent=new Intent();
		intent.setAction(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
		
	}
	
}
