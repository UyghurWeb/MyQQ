package com.zsl.myqq;

import java.util.Collection;
import java.util.List;

import javax.security.auth.callback.Callback;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;

import com.zsl.adapter.ElsAdapter;
import com.zsl.smack.Smack;
import com.zsl.util.AllServicesControl;
import com.zsl.util.MySession;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceActivity.Header;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.app.Activity;
import android.content.Intent;

/**
 * 联系人
 * @author ZSL
 * @version 2014-6-10 14:01:34
 */
public class ContantsActivity extends Activity {
	private ExpandableListView els;
	private ImageView iv_addUser;
	private ImageView iv_addGroup;
	private Handler handler;
	private List<RosterGroup> rgList;
	private AllServicesControl allServicesControl=new AllServicesControl();
	private Smack smack=LoginActivity.getSmack();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_contants);
		init();
		
		iv_addUser.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(ContantsActivity.this, AddUserActivity.class);
				startActivity(intent);
				ContantsActivity.this.finish();
			}
		});
		
		iv_addGroup.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(ContantsActivity.this, AddGroupActivity.class);
				startActivity(intent);
				ContantsActivity.this.finish();
			}
		});
		
		els=(ExpandableListView) findViewById(R.id.tab_contants_expandableListView);
		if(smack.getGroup("我的好友")){
			boolean aa=smack.addGroup("我的好友");
			System.out.println("======添加组"+aa);
		}
		rgList=smack.getRosterGroup();
		
		final ElsAdapter adapter=new ElsAdapter(rgList,this);
		els.setAdapter(adapter);
		
		els.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView arg0, View arg1, int arg2,
					int arg3, long arg4) {
				RosterEntry rosterEntry=(RosterEntry) adapter.getChild(arg2, arg3);
				MySession mySession=MySession.getMySession();
				mySession.put("chat_contant",rosterEntry );
				Intent intent=new Intent(ContantsActivity.this, ChatActivity.class);
				startActivity(intent);
				return false;
			}
		});
		
		
		smack.getConnection().getRoster().addRosterListener(new RosterListener() {
			
			@Override
			public void presenceChanged(Presence arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void entriesUpdated(Collection<String> arg0) {
				// TODO Auto-generated method stub
				rgList=smack.getRosterGroup();
				adapter.notifyDataSetChanged();
				//启动聊天消息服务
				startService(allServicesControl.startChatMsgServices());
			}
			
			@Override
			public void entriesDeleted(Collection<String> arg0) {
				// TODO Auto-generated method stub
				
				rgList=smack.getRosterGroup();
				adapter.notifyDataSetChanged();
				//启动聊天消息服务
				startService(allServicesControl.startChatMsgServices());
			}
			
			@Override
			public void entriesAdded(Collection<String> arg0) {
				// TODO Auto-generated method stub
				rgList=smack.getRosterGroup();
				adapter.notifyDataSetChanged();
				//启动聊天消息服务
				startService(allServicesControl.startChatMsgServices());
			}
		});
	}
	
	/**
	 * 初始化
	 */
	public void init(){
		iv_addUser=(ImageView) findViewById(R.id.tab_contants_top_iv_addUser);
		iv_addGroup=(ImageView) findViewById(R.id.tab_contants_top_iv_addGroup);
	}
	
	
}
