package com.zsl.myqq;


import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;

import com.zsl.adapter.MsgAdapter;
import com.zsl.bean.Msg;
import com.zsl.dao.MessageDao;
import com.zsl.dao.impl.MessageDaoImpl;
import com.zsl.smack.Smack;
import com.zsl.util.AllServicesControl;
import com.zsl.util.MySession;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.sax.StartElementListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;



public class MsgActivity extends Activity {
	//控件
	private ListView lv_msg; 
	//属性
	private Smack smack=LoginActivity.getSmack();
	private XMPPConnection xmppconn;
	private MessageDao messageDao;
	private AllServicesControl allServicesControl=new AllServicesControl();
	private boolean run=true;
	private Thread thread;
	private Handler handler;
	private MsgAdapter adapter;
	private MySession mySession;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_msg);
		init();
		xmppconn=smack.getConnection();
		messageDao=new MessageDaoImpl(this);
		
		mySession=MySession.getMySession();
		mySession.put("context", MsgActivity.this);
		//启动聊天消息服务
		startService(allServicesControl.startChatMsgServices());
		
		//查询消息
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
				case 1:
				{
					String uname=(String) mySession.get("me_uname");
					final String toto=uname+"@"+xmppconn.getServiceName().toString();
					Msg msgss=new Msg();
					msgss.setMto(toto);
					List<Msg> list=messageDao.findAllByGroup(msgss);
					for (int i = 0; i < list.size(); i++) {
						System.out.println("server======msg:"+list.get(i).getMsg());
					}
					adapter=new MsgAdapter(list, MsgActivity.this);
					lv_msg.setAdapter(adapter);
				}
				break;

				default:
					break;
				}
				
			}
		};
		
		//查询未读消息的线程
		thread=new Thread(){
			public void run() {
				while(run){
					try {
						thread.sleep(3000);
						handler.sendEmptyMessage(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}
			};
		};
		//启动线程
		thread.start();

		lv_msg.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				System.out.println("======点击了消息相");
				Msg msg=(Msg) adapter.getItem(arg2);
				String fname=msg.getMfrom().toString();
				mySession.put("msg_start_from",fname );
				List<RosterEntry> entries=smack.getEntries();
				for (int i = 0; i < entries.size(); i++) {
					String user=entries.get(i).getUser().toString();
					if (user.equals(fname)) {
						mySession.put("chat_contant",entries.get(i) );
						Intent intent=new Intent(MsgActivity.this, ChatActivity.class);
						startActivity(intent);
						break;
					}
				}
				
			}
		});

	}
	/**
	 * 初始化
	 */
	public void init(){
		lv_msg=(ListView) findViewById(R.id.tab_msg_listView);
	}
}
