package com.zsl.myqq;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;


/**
 * 工具界面
 * @author ZSL
 * @version 2014-6-22 9:13:07
 *
 */
public class ToolsActivity extends Activity implements OnClickListener{
	private RelativeLayout rl_wifi;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_tools);
		init();
	}
	/**
	 * 初始化
	 */
	private void init(){
		rl_wifi=(RelativeLayout) findViewById(R.id.tab_tools_rl_wifi);
		rl_wifi.setOnClickListener(this);
	}
	/**
	 * 点击事件
	 */
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		//wifi破解
		case R.id.tab_tools_rl_wifi:
		{
			System.out.println("wifi");
			Intent intent=new Intent(ToolsActivity.this, WifiMainActivity.class);
			startActivity(intent);
		}
		break;

		default:
			break;
		}
	};
	
}
