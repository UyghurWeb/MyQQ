package com.zsl.myqq;

import java.util.List;

import com.zsl.adapter.WifiAdapter;
import com.zsl.util.WifiAdmin;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 应用主界面
 * 
 * @author ZSL
 * @version 2014年5月16日 14:51:29
 */
public class WifiMainActivity extends Activity {
	static final int NOTIFICTION_ID=0x1123;
	// 定义控件
	private static int off = 0;
	private Button bt_open;
	private Button bt_close;
	private Button bt_state;
	private Button bt_list;
	private TextView tv_pass;
	private TextView tv_cepass;
	private ListView lv_wifi;
	private TextView tv_count;
	private ImageView iv_back;
	private SharedPreferences sp;
	// 定义wifiManager
	private static WifiAdmin admin;
	// 定义扫描结果列表
	private List<ScanResult> wlist;
	private ScanResult scanResult;
	private List<WifiConfiguration> wConfigList;
	private static WifiConfiguration wConfig = new WifiConfiguration();

	private WifiAdapter adapter;

	private Handler handler;
	private Handler handler1;
	private Handler handler2;
	private Handler handler3;
	private Runnable r = null;
	private Runnable r1 = null;
	private Runnable r2 = null;
	private Runnable r3 = null;

	private StringBuffer sb = new StringBuffer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wifi_main);
		bt_open = (Button) findViewById(R.id.wifi_main_bt_open);
		bt_close = (Button) findViewById(R.id.wifi_main_bt_close);
		bt_state = (Button) findViewById(R.id.wifi_main_bt_state);
		bt_list = (Button) findViewById(R.id.wifi_main_bt_list);
		tv_pass = (TextView) findViewById(R.id.wifi_main_tv_pass);
		lv_wifi = (ListView) findViewById(R.id.wifi_main_lv_wifi);
		tv_count = (TextView) findViewById(R.id.wifi_main_tv_count);
		tv_cepass = (TextView) findViewById(R.id.wifi_main_tv_cepass);
		iv_back=(ImageView) findViewById(R.id.wifi_top_iv_back);
		
		admin = new WifiAdmin(this);
		
		iv_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
				WifiMainActivity.this.finish();
			}
		});
		// 打开wifi
		bt_open.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int state = admin.checkState();
				if (state == 1) {
					admin.openWifi();
					Toast.makeText(WifiMainActivity.this, "Wifi 打开成功",
							Toast.LENGTH_SHORT).show();
				} else if (state == 2) {
					Toast.makeText(WifiMainActivity.this, "Wifi打开中...",
							Toast.LENGTH_SHORT).show();
				} else if (state == 3) {
					Toast.makeText(WifiMainActivity.this, "Wifi已打开",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		bt_close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int state = admin.checkState();
				if (state == 3) {
					if (off == 1) {
						Toast.makeText(WifiMainActivity.this,
								"Wifi 密码破解中,请勿关闭Wifi", Toast.LENGTH_SHORT)
								.show();
					} else {
						admin.closeWifi();
						Toast.makeText(WifiMainActivity.this, "Wifi 关闭成功",
								Toast.LENGTH_SHORT).show();
					}
				} else if (state == 2) {
					Toast.makeText(WifiMainActivity.this, "Wifi打开中...",
							Toast.LENGTH_SHORT).show();
				} else if (state == 1) {
					Toast.makeText(WifiMainActivity.this, "Wifi已关闭",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		bt_state.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(WifiMainActivity.this,
						"当前wifi状态为：" + admin.checkState(), Toast.LENGTH_SHORT)
						.show();
			} 
		});

		bt_list.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int state = admin.checkState();
				if (state == 3) {

					getAllNetWorkList();
					Toast.makeText(WifiMainActivity.this, "扫描完成",
							Toast.LENGTH_SHORT).show();
				} else if (state == 2) {
					Toast.makeText(WifiMainActivity.this, "Wifi打开中...",
							Toast.LENGTH_SHORT).show();
				} else if (state == 1) {
					Toast.makeText(WifiMainActivity.this, "请打开Wifi",
							Toast.LENGTH_SHORT).show();
				}
				

			}
		});
		// 个数消息更新
		handler = new Handler(new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub
				if (msg != null) {
					tv_count.setText((msg.obj).toString());
				}

				return false;
			}
		});
		// 破解密码消息
		handler1 = new Handler(new Handler.Callback() {

			@SuppressWarnings("deprecation")
			@Override
			public boolean handleMessage(Message msg) {

				if (msg != null) {
					tv_cepass.setText("破解密码为：");
					tv_pass.setText((msg.obj).toString());
					/**
					 * 消息通知
					 */
					//创建一个启动其他Activityde intent
					Intent intent=new Intent(WifiMainActivity.this, WifiMainActivity.class);
					PendingIntent pendingIntent=PendingIntent.getActivity(WifiMainActivity.this, 0, intent, 0);
					
					//创建一个Notification
					Notification notification=new Notification();
					//设置图标
					notification.icon=R.drawable.wifi_ic_launcher;
					//设置通知内容
					notification.tickerText="启动一个其他的Activity";
					//设置发送时间
					notification.when=System.currentTimeMillis();
					//设置声音
					notification.defaults=Notification.DEFAULT_SOUND;
					//设置默认声音、振动、闪光
					notification.defaults=Notification.DEFAULT_ALL;
					//设置事件信息
					notification.setLatestEventInfo(WifiMainActivity.this, "Wifi密码破解成功", "点击查看", pendingIntent);
					
					//获取系统的NotificationManager服务
					NotificationManager manager=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
					//
					manager.notify(NOTIFICTION_ID, notification);
				}
				return false;
			}
		});
		// 测试密码消息
		handler2 = new Handler(new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub
				if (msg != null) {
					tv_cepass.setText("当前测试密码为：");
					tv_pass.setText((msg.obj).toString());
				}
				return false;
			}
		});

		// 获取破解密码消息
		handler3 = new Handler(new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub

				if (msg != null) {
					ScanResult result = (ScanResult) msg.obj;
					sp = getSharedPreferences(result.SSID, MODE_APPEND);
					String name = sp.getString("name", result.SSID + ":");
					String pass = sp.getString("pass", "此Wifi未破解！");

					tv_cepass.setText(name+":");
					tv_pass.setText(pass);
				}
				return false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// 扫描Wifi密码
	public void getAllNetWorkList() {
		// 每次点击清空上次扫描记录
		if (sb != null) {
			sb = new StringBuffer();
		}
		// 开始扫描网络
		admin.startScan();
		wlist = admin.getWifiList();
		adapter = new WifiAdapter(this, wlist);
		if (wlist != null) {
			for (int i = 0; i < wlist.size(); i++) {
				scanResult = wlist.get(i);

				sb = sb.append(scanResult.BSSID + " ")
						.append(scanResult.SSID + " ")
						.append(scanResult.capabilities + " ")
						.append(scanResult.frequency + " ")
						.append(scanResult.level + " \n\n");
			}
			System.out.println("扫描到的wifi网络：\n" + sb.toString());
		}

		lv_wifi.setAdapter(adapter);
		lv_wifi.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				scanResult = wlist.get(arg2);
				String[] content = new String[] { "获取已破解密码", "暴力破解" };
				AlertDialog.Builder ab = new Builder(WifiMainActivity.this);
				ab.create();
				ab.setTitle("请选择连接方式");
				ab.setItems(content, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						switch (which) {
						case 0: {

							r3 = new Runnable() {
								@Override
								public void run() {
									Message msg = handler3.obtainMessage();
									msg.obj = scanResult;
									handler3.sendMessage(msg);// 发送消息给Handler
								}
							};

							handler3.post(r3);

						}
							break;
						case 1: {
							if (off == 0) {
								off = 1;
								AlertDialog.Builder ab1 = new Builder(
										WifiMainActivity.this);
								
								ab1.create();
								ab1.setTitle("破解中...");
								LinearLayout layout = new LinearLayout(
										WifiMainActivity.this);
								layout.setGravity(Gravity.CENTER_HORIZONTAL);
								ProgressBar progressBar = new ProgressBar(
										WifiMainActivity.this);
								layout.addView(progressBar);
								ab1.setView(layout);
								ab1.setMessage("破解时间可能比较长，请放入后台破解,破解成功后，程序会通知您的哦");
								ab1.setPositiveButton("确定",new android.content.DialogInterface.OnClickListener (){

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										// TODO Auto-generated method stub
										Intent intent=new Intent();
										intent.setAction(Intent.ACTION_MAIN);
										intent.addCategory(Intent.CATEGORY_HOME);
										startActivity(intent);
									}
									
								});
								ab1.show();

								new Thread() {
									public void run() {
										try {
											Thread.sleep(3000);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										final String pass = getPass(scanResult);
										off = 0;
										String msgs = "";
										if ("".equals(pass)
												|| pass.equals(null)) {
											msgs = "Wifi密码破解失败!_!";
										} else {

											msgs = "Wifi密码破解成功^_^";

											r1 = new Runnable() {
												@Override
												public void run() {
													Message msg = handler1
															.obtainMessage();
													msg.obj = "" + pass;
													handler1.sendMessage(msg);// 发送消息给Handler

												}
											};

											handler1.post(r1);
										}

										final String msgss = msgs;

										r = new Runnable() {

											@Override
											public void run() {
												// TODO Auto-generated method
												// stub
												Message msg = handler
														.obtainMessage();
												msg.obj = msgss;
												handler.sendMessage(msg);// 发送消息给Handler
											}
										};

										handler.post(r);

									};
								}.start();
							} else {
								Toast.makeText(WifiMainActivity.this, "正在破解中。。。",
										Toast.LENGTH_LONG).show();
							}
						}
							break;

						default:
							break;
						}
					}
				});
				ab.show();
				return false;
			}
		});

	}

	// 暴力破解方法
	public String getPass(ScanResult scanResult) {
		String[] aa = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a",
				"b", "c", "d", "e", "f", "j", "h", "i", "g", "k", "l", "m",
				"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y",
				"z", "A", "B", "C", "D", "E", "F", "J", "H", "I", "G", "K",
				"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
				"X", "Y", "Z" };
		// 位数
		int ws = aa.length;

		String mima = "";

		String SSID = scanResult.SSID;
		wConfig.SSID = SSID;
		for (int f = 8; f < ws + 2; f++) {

			System.out.println("长度为：" + f);

			int h = 1;
			for (int i = 0; i < f; i++) {
				h = h * ws;
				System.out.println("wswswswswswswswsws:" + h);
			}
			int[] name = new int[f];
			for (int g = 0; g < f; g++) {
				name[g] = -1;
			}
			System.out.println("namename" + name.length);

			for (int j = 0; j < h; j++) {
				mima = "";
				int[] val = new int[f];
				for (int m = 0; m < val.length; m++) {
					if (m == 0) {
						val[0] = j % ws;
						name[0] = j % ws;
						continue;
					} else {

						int wss = 1;
						for (int n = 0; n < m; n++) {
							wss = wss * ws;
						}
						val[m] = j % wss;
					}
				}

				for (int u = 1; u < f; u++) {
					if (val[u] == 0) {
						name[u] = (name[u] + 1) % ws;
					}
				}

				for (int i = 0; i < f; i++) {
					int g = name[i];
					mima = mima + aa[g];
				}
				System.out.println("个数：" + j);
				final String count = "" + j;
				System.out.println(mima);

				r = new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						Message msg = handler.obtainMessage();
						msg.obj = count;
						handler.sendMessage(msg);// 发送消息给Handler
					}
				};

				handler.post(r);

				final String Pass = "" + mima;
				r2 = new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						Message msg = handler2.obtainMessage();
						msg.obj = "" + Pass;
						handler2.sendMessage(msg);// 发送消息给Handler
					}
				};

				handler2.post(r2);
//				测试密码破解后结果
				if (mima.equals("jk100000")) {
					System.err.println("密码为：" + mima);
					sp = getSharedPreferences(SSID, MODE_APPEND);
					Editor editor = sp.edit();
					editor.putString("name", SSID);
					editor.putString("pass", mima);
					editor.commit();
					return mima;
				}
//				if (connectionWifi(mima)) {
//					System.err.println("密码为：" + mima);
//					sp = getSharedPreferences(SSID, MODE_APPEND);
//					Editor editor = sp.edit();
//					editor.putString("name", SSID);
//					editor.putString("pass", mima);
//					editor.commit();
//					return mima;
//				}
			}
		}
		return "";
	}

	public boolean connectionWifi(String pass) {
		wConfig.preSharedKey = pass;
		return admin.addNetWork(wConfig);
	}

}
