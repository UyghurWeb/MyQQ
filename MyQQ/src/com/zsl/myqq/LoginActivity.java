package com.zsl.myqq;

import org.jivesoftware.smack.XMPPException;

import com.zsl.smack.Smack;
import com.zsl.smack.SmackImpl;
import com.zsl.util.MySession;
import com.zsl.util.Valdate;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener{
	private EditText et_name;
	private EditText et_pass;
	private Button bt_login;
	private ImageView top_iv_reg;
	private TextView top_tv_reg;
	private Thread th_login;
	private static Smack smack=new SmackImpl();
	private Handler handler;
	private Dialog dialog;
	private LayoutInflater inflater;
	private SharedPreferences sp;
	private String name;
	private String pass;
	
	public static Smack getSmack() {
		return smack;
	}
	public static void setSmack(Smack smack) {
		LoginActivity.smack = smack;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		init();
		sp=getSharedPreferences("logininfo", MODE_WORLD_READABLE
				| MODE_WORLD_WRITEABLE);
		String spuname = sp.getString("uname", null);
		String spupass = sp.getString("upass", null);
		et_name.setText(spupass);
		et_pass.setText(spupass);
		bt_login.setOnClickListener(this);
		top_iv_reg.setOnClickListener(this);
		top_tv_reg.setOnClickListener(this);
		inflater=LayoutInflater.from(this);
		handler=new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
				//打开登陆提示
				case 1:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					dialog=new Dialog(LoginActivity.this);
					dialog.setTitle("登录提示");
					RelativeLayout ll=(RelativeLayout) inflater.inflate(R.layout.toast, null);
					TextView tv=(TextView) ll.findViewById(R.id.toast_text);
					tv.setText("登录中...");
					dialog.setContentView(ll);
					dialog.show();
				}
				break;
				//登陆成功，关闭登陆提示
				case 2:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(LoginActivity.this, "登录成功！", Toast.LENGTH_SHORT).show();
					Editor editor=sp.edit();
					editor.putString("uname",name );
					editor.putString("upass", pass);
					editor.commit();
					
					Intent intent1=new Intent(LoginActivity.this, TabMainActivity.class);
					startActivity(intent1);
					LoginActivity.this.finish();
				}
				break;
				case 3:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(LoginActivity.this, "登录失败！", Toast.LENGTH_SHORT).show();
				}
				break;
				case 4:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(LoginActivity.this, "帐号不能为空", Toast.LENGTH_LONG).show();
				}
				break;
//				密码不能为空
				case 5:
				{
					if (dialog!=null&&dialog.isShowing()) {
						dialog.dismiss();
					}
					Toast.makeText(LoginActivity.this, "密码不能为空", Toast.LENGTH_LONG).show();
				}
				break;
				
				default:
					break;
				}
			}
		};
		
	}
	/**
	 * 控件初始化
	 */
	private void init(){
		et_name=(EditText) findViewById(R.id.login_et_name);
		et_pass=(EditText) findViewById(R.id.login_et_pass);
		bt_login=(Button) findViewById(R.id.login_bt_login);
		top_iv_reg=(ImageView) findViewById(R.id.login_top_iv_tab_me);
		top_tv_reg=(TextView) findViewById(R.id.login_top_tv_reg);
		
	}
	
	/**
	 * 点击事件
	 */
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.login_bt_login:
		{
			name=et_name.getText().toString();
			pass=et_pass.getText().toString();
			
			
			/**
			 * 起一个登陆线程
			 */
			th_login=new Thread(){@Override
				public void run() {
				handler.sendEmptyMessage(1);
				
					// TODO Auto-generated method stub
					try {
						smack.login(name, pass);
						MySession mySession=MySession.getMySession();
						mySession.put("me_uname", name);
						handler.sendEmptyMessage(2);
					} catch (XMPPException e) {
						handler.sendEmptyMessage(3);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}};
				
				Valdate valdate=new Valdate();
				if(valdate.valdates(name)){
					if (valdate.valdates(pass)) {
						System.out.println(name+":::"+pass);
						th_login.start();
					} else {
						handler.sendEmptyMessage(5);
					}
				}else{
					handler.sendEmptyMessage(4);
				}
				
		}
		break;
		
		case R.id.mevcard_top_iv_tab_me:
		{
			smack.logout();
			Intent intent=new Intent(LoginActivity.this, RegActivity.class);
			
			startActivity(intent);
			this.finish();
		}
		break;
		case R.id.login_top_tv_reg:
		{
			smack.logout();
			Intent intent=new Intent(LoginActivity.this, RegActivity.class);
			
			startActivity(intent);
			this.finish();
		}
		break;
		default:
			break;
		}
	}
	/**
	 * 按两次返回键退出
	 */
	private long firsttime;
	@Override
	public void onBackPressed() {
		if (System.currentTimeMillis()- firsttime <3000) {
			this.finish();
		} else {
			firsttime=System.currentTimeMillis();
			Toast.makeText(this, "再按一次退出哦", Toast.LENGTH_LONG).show();
		}
	}
}
