package com.zsl.myqq;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

import com.zsl.adapter.ChatAdapter;
import com.zsl.adapter.MsgAdapter;
import com.zsl.bean.Msg;
import com.zsl.dao.MessageDao;
import com.zsl.dao.impl.MessageDaoImpl;
import com.zsl.smack.Smack;
import com.zsl.util.AllServicesControl;
import com.zsl.util.MySession;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


/**
 * 聊天界面
 * @author ZSL
 * @version 2014-6-16 16:50:39
 */
public class ChatActivity extends Activity {
	//===控件
	private Button bt_send;
	private EditText et_msg;
	private ListView lv_msg;
	private TextView tv_title;
	private ImageView iv_back;
	//=====属性
	private Chat chat;
	private Smack smack=LoginActivity.getSmack();
	private XMPPConnection connection;
	private List<HashMap< String, Object>> lsmsg;
	private int []icon={R.drawable.ic_launcher,R.drawable.login_default_avatar};
	private int []layout={R.layout.chat_item_left,R.layout.chat_item_right};
	private int []layout_id={R.id.chat_item_left_icon,R.id.chat_item_left_tv_msg,R.id.chat_item_left_datetime,R.id.chat_item_right_icon,R.id.chat_item_right_tv_msg,R.id.chat_item_right_datetime};
	private AllServicesControl allServicesControl=new AllServicesControl();
	
	private ChatAdapter adapter;
	private Handler handler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat);
		init();
		connection=smack.getConnection();
		final MessageDao messageDao=new MessageDaoImpl(this);
//		
		//聊天
		lsmsg=new ArrayList<HashMap<String,Object>>();
		adapter=new ChatAdapter(lsmsg, layout, layout_id, this);
		lv_msg.setAdapter(adapter);
		MySession mySession=MySession.getMySession();
		RosterEntry rosterEntry=(RosterEntry) mySession.get("chat_contant");
		tv_title.setText(rosterEntry.getName().toString());
		
		chat = connection.getChatManager().createChat(rosterEntry.getUser().toString(), new MessageListener() {
			@Override
			public void processMessage(Chat arg0, Message arg1) {
				// TODO Auto-generated method stub
				System.out.println("other=======message: " + arg1.getBody());
				addMessage(arg1.getBody().toString(), 0,"");
				adapter.notifyDataSetChanged();
				lv_msg.setSelection(lsmsg.size());
			}
			
		 });
		
		
		bt_send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String msg=et_msg.getText().toString();
				try {
					if (!"".equals(msg)&&!msg.equals(null)) {
						chat.sendMessage(msg);
						addMessage(msg, 1,"");
						et_msg.setText("");
						adapter.notifyDataSetChanged();
						lv_msg.setSelection(lsmsg.size());
					}
				} catch (XMPPException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		iv_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				onBackPressed();
				
			}
		});
		//离线消息查询
		handler=new Handler(){
			@Override
			public void handleMessage(android.os.Message msg) {
				switch (msg.what) {
				case 1:
				{
					MySession mySession=MySession.getMySession();
					String uname=(String) mySession.get("me_uname");
					String fname=(String) mySession.get("msg_start_from");
					if (!"".equals(fname)&&fname!=null) {
						final String toto=uname+"@"+connection.getServiceName().toString();
						Msg msgss=new Msg();
						msgss.setMto(toto);
						msgss.setMfrom(fname);
						List<Msg> list=messageDao.findAll(msgss);
						for (int i = 0; i < list.size(); i++) {
							addMessage(list.get(i).getMsg().toString(), 0,list.get(i).getMdate().toString());
						}
						boolean start=false;
						while(!start){
							
							start=messageDao.update(msgss);
						}
						
						adapter.notifyDataSetChanged();
						
						lv_msg.setSelection(lsmsg.size());
					}
					
				}
				break;
				
				default:
					break;
				}
			}
		};
		
		//起一个查询未读消息的线程
		new Thread(){
			public void run() {
				handler.sendEmptyMessage(1);
				};
		}.start();
	}
	/**
	 * 添加消息
	 */
	public void addMessage(String msg,int who,String time){
		HashMap<String, Object> message=new HashMap<String, Object>();
		message.put("who", who);
		message.put("msg", msg);
		message.put("image",icon[who==0?0:1]);
		if (time.equals("")) {
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			message.put("time",dateFormat.format(new Date()));
		}else{
			message.put("time", time);
		}
		lsmsg.add(message);
	}
	
	/**
	 * 初始化
	 */
	public void init (){
		lv_msg=(ListView) findViewById(R.id.chat_msg_listView1);
		et_msg=(EditText) findViewById(R.id.chat_input);
		bt_send=(Button) findViewById(R.id.chat_send);
		tv_title=(TextView) findViewById(R.id.chat_title);
		iv_back=(ImageView) findViewById(R.id.chat_top_iv_tab_me);
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		startService(allServicesControl.startChatMsgServices());
		super.onBackPressed();
		ChatActivity.this.finish();
	}

	
}
