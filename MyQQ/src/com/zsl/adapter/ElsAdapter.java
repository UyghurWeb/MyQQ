package com.zsl.adapter;

//import com.example.expanablelistview.R;

import java.util.List;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;

import com.zsl.myqq.LoginActivity;
import com.zsl.myqq.R;
import com.zsl.smack.Smack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ElsAdapter extends BaseExpandableListAdapter {
	private List<RosterGroup> rglList;
	private Smack smack=LoginActivity.getSmack();
	private LayoutInflater inflater;
	private Context context;
	
	
	public ElsAdapter(List<RosterGroup> rgList, Context context) {
		super();
		this.rglList=rgList;
		this.setContext(context);
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		
		
		return smack.getEntriesByGroup(rglList.get(groupPosition).getName().toString()).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}
	public static class ViewHolderChild{
		TextView textView;
	}
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
			ViewHolderChild viewHolderChild;
			if (convertView==null) {
				viewHolderChild=new ViewHolderChild();
				convertView=inflater.inflate(R.layout.contact_list_item_for_buddy, null);
				viewHolderChild.textView=(TextView) convertView.findViewById(R.id.contact_list_item_name);
				convertView.setTag(viewHolderChild);
			} else {
				viewHolderChild=(ViewHolderChild) convertView.getTag();
			}
			RosterEntry rosterEntry=(RosterEntry) getChild(groupPosition, childPosition);
			if (rosterEntry!=null) {
				viewHolderChild.textView.setText(rosterEntry.getName().toString());
			}
			
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		System.out.println("=======name"+rglList.get(groupPosition).getName());
		return smack.getEntriesByGroup(rglList.get(groupPosition).getName().toString()).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return rglList.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return rglList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}
	
	public static class ViewHolderGroup{
		TextView textView;
	}
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		ViewHolderGroup viewHolderGroup ;
		if (convertView==null) {
			convertView=inflater.inflate(R.layout.contact_buddy_list_group, parent,false);
			viewHolderGroup=new ViewHolderGroup();
			viewHolderGroup.textView=(TextView) convertView.findViewById(R.id.group_name);
			convertView.setTag(viewHolderGroup);
			
		} else {
			viewHolderGroup=(ViewHolderGroup) convertView.getTag();
		}
		RosterGroup rosterGroup=rglList.get(groupPosition);
		if (rosterGroup!=null) {
			viewHolderGroup.textView.setText(rosterGroup.getName());
		}
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}


}
