package com.zsl.adapter;

import java.util.List;

import com.zsl.myqq.R;



import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class WifiAdapter extends BaseAdapter {
	private Context context;
	private List<ScanResult> wlist;
	private LayoutInflater inflater;

	public WifiAdapter(Context context, List<ScanResult> wlist) {
		super();
		this.context = context;
		this.wlist = wlist;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return wlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return wlist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	// �����ؼ�������
	public class ViewHolder {
		private TextView tv_uname;
		private TextView tv_upass;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh=null;
		if (convertView==null) {
			vh=new ViewHolder();
			convertView=inflater.inflate(R.layout.wifi_list_item, null);
			vh.tv_uname=(TextView) convertView.findViewById(R.id.wifi_list_item_tv_uname);
			vh.tv_upass=(TextView) convertView.findViewById(R.id.wifi_list_item_tv_upass);
			convertView.setTag(vh);
			
		} else {
			vh=(ViewHolder) convertView.getTag();
		}
		
		ScanResult scan=wlist.get(position);
		vh.tv_uname.setText(scan.SSID.toString());
		vh.tv_upass.setText(scan.capabilities.toString());
		
		
		return convertView;
	}

}
